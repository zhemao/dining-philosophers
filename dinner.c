#include "dinner.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void dine(philosopher_t * philosopher){
    int myforks[2];
    int goal;

    if( philosopher->energy == MAX_ENERGY )
        return;

    if(philosopher->id == (TABLEN - 1)){
        myforks[0] = 0;
        myforks[1] = philosopher->id;
    } else {
        myforks[0] = philosopher->id;
        myforks[1] = philosopher->id + 1;
    }

    if( pthread_mutex_trylock(forks + myforks[0]) != 0 )
        return;

    if( pthread_mutex_trylock(forks + myforks[1]) != 0 ){
        pthread_mutex_unlock(forks + myforks[0]);
        return;
    }

    if( philosopher->energy < ENERGY_THRESHHOLD )
        goal = ENERGY_THRESHHOLD;
    else {
        goal = philosopher->energy + ENERGY_STEP;
        
        if( goal > MAX_ENERGY )
            goal = MAX_ENERGY;
    }

    printf("%s is dining.\n", names[philosopher->id]);

    usleep( (goal - philosopher->energy) * USEC_PER_UNIT );

    philosopher->energy = goal;

    pthread_mutex_unlock(forks + myforks[1]);
    pthread_mutex_unlock(forks + myforks[0]);
}

void ponder(philosopher_t * philosopher){
    if( philosopher->wisdom > (MAX_WISDOM - WISDOM_STEP) ){
        float ratio = (float) ENERGY_STEP / (float) WISDOM_STEP;
        usleep( USEC_PER_UNIT * (MAX_WISDOM - philosopher->wisdom) );
        philosopher->energy -= (MAX_WISDOM - philosopher->wisdom) * ratio;
        philosopher->wisdom = MAX_WISDOM;
    } else {
        usleep( USEC_PER_UNIT * WISDOM_STEP );
        philosopher->energy -= ENERGY_STEP;
        philosopher->wisdom += WISDOM_STEP;
    }
}

void* philosophize(void * thread_data){
    philosopher_t * philosopher = (philosopher_t*) thread_data;
    
    while( philosopher->energy > 0 && philosopher->wisdom < MAX_WISDOM ){
        dine(philosopher);
        ponder(philosopher);
    }

    if( philosopher->energy <= 0 ){
        printf("%s has starved to death.\n", names[philosopher->id]);
    } else if( philosopher->wisdom >= MAX_WISDOM ){
        printf("%s has attained enlightenment.\n", names[philosopher->id]);
    }

    pthread_exit(NULL);
}

int main(void){
    philosopher_t philosophers[TABLEN];
    int i;

    for(i=0; i<TABLEN; i++){
        pthread_mutex_init( forks + i, NULL );
    }

    for(i=0; i<TABLEN; i++){
        philosophers[i].id = i;
        philosophers[i].wisdom = 0;
        philosophers[i].energy = MAX_ENERGY;
        pthread_create(&philosophers[i].thread, NULL, 
                philosophize, (void*) &philosophers[i]);
    }

    for(i=0; i<TABLEN; i++){
        pthread_join(philosophers[i].thread, NULL);
    }

    for(i=0; i<TABLEN; i++){
        pthread_mutex_destroy( forks + i );
    }

    return 0;
}
