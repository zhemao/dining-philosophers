#ifndef __DINNER_H__
#define __DINNER_H__

#include <pthread.h>

#define TABLEN 12
#define MAX_WISDOM 100
#define WISDOM_STEP 10
#define MAX_ENERGY 100
#define ENERGY_THRESHHOLD 65
#define ENERGY_STEP 10
#define USEC_PER_UNIT 100000

pthread_mutex_t forks[TABLEN];

const char * names[TABLEN] = {
    "Plato",
    "Descartes",
    "Locke",
    "Sartre",
    "Nietzche",
    "Kant",
    "Spinoza",
    "Aristotle",
    "Camus",
    "Diogenes",
    "Russell",
    "Wittgenstein"
};

typedef struct philosopher {
    pthread_t thread;
    int id;
    int wisdom;
    int energy;
} philosopher_t;

void dine(philosopher_t * philosopher);
void ponder(philosopher_t * philosopher);
void* philosophize(void * thread_data);

#endif
