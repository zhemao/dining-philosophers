CC=gcc
CFLAGS=-Wall -O2
LDFLAGS=-lpthread

dinner: dinner.c dinner.h
	$(CC) $(CFLAGS) $< $(LDFLAGS) -o $@
